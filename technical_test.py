
###
# Author:   Tanay Reddy Nagireddy
# Date:     October 20th, 2020
# Title:    Covid API
###


from logging import error
import yaml, logging, requests, sys
import pandas as pd


# making api request and storing found results
def request_data(results, date, iso, ind, logger_):
    response = requests.get("https://covid-api.com/api/reports?date={}&iso={}".format(date, iso))
    if response.json()["data"] != []:  # len(response.json()["data"])
        data = response.json()["data"][0]
        results.loc[ind] = [
            data["date"],
            data["region"]["iso"],
            data["confirmed"],
            data["deaths"],
            data["recovered"]
        ]
        logger_.info("{} found and appended".format(data["region"]["name"]))
        return "found and appended"
    elif response.json()["data"] == []:
        return "empty"


if __name__ == "__main__":

    # logger setup
    FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")
    LOG_FILE = R"C:\Users\tanay\OneDrive\Desktop\Exiger Technical Test\covid_datarequests_log.log"

    # console and file handler
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    file_handler = logging.FileHandler(LOG_FILE)
    file_handler.setFormatter(FORMATTER)

    # set logger object
    logger = logging.getLogger("technical_test")
    logger.setLevel(logging.DEBUG)
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)

    # set config file path
    config_file_path = R"C:\Users\tanay\OneDrive\Desktop\Exiger Technical Test\config_.yaml"

    # set config context and read from config file
    config = yaml.safe_load(open(config_file_path))
    logger.info("config file set")

    # reading the excel file containing api request paramters - iso data taken from https://www.iso.org/obp/ui/#search
    excel_file = pd.read_excel(config["file_path"])
    logger.info("api request parameters retrieved")

    # create data table
    covid_data = pd.DataFrame(columns=["date", "iso", "num_confirmed", "num_deaths", "num_recovered"])

    # api request with error handling
    try:
        for row in excel_file.itertuples(index=True):
            request_data(results=covid_data, date=row.date.date(), iso=row.iso, ind=row.Index, logger_=logger)
    except Exception as error:
        logger.error("api request failed due to the following error:\n\n{}".format(error))
        # send error notification via slack, email, datadog, etc. 

    print(covid_data.head(5))

    # removing blank rows
    covid_data.dropna(axis=0, inplace=True)

    # storing results to disk
    covid_data.to_excel("covid19_statsbycountry.xlsx", index=False)
    logger.info("data saved to excel file on disk")

