# COVID19 Data by Country

This script retrieves the following covid19 stats from covid-api.com by country:

* Number of confirmed cases
* Number of deaths
* Number of recovered cases

The following actions have been performed:

1. call file path to excel containing api request parameters from config file (config_.yaml)
2. set up respective log for technical_test.py as well as unit_test.py
3. make api request for the above mentioned values
4. clean up result set, i.e., remove empty rows
5. store results to disk "covid19_statsbycountry.xlsx"
6. api request unit tests are performed in the unit_test.py file

## Version

covid-api-v1.0.0 using python==3.8.3.

## Usage



The script can be run from command line with:

    $ python  "~/technical_test.py"

Unit tests can be run from command line with:

    $ python  "~/unit_test.py"

where "python" is mapped to the interpreter executable.


## Contributing
For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.