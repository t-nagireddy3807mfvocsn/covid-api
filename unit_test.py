from datetime import datetime
from types import ClassMethodDescriptorType
import unittest, logging, sys
import pandas as pd
from technical_test import request_data

# logger setup
FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")
LOG_FILE = R"C:\Users\tanay\OneDrive\Desktop\Exiger Technical Test\unit_tests_log.log"

# console and file handler
console_handler = logging.StreamHandler(sys.stdout)
console_handler.setFormatter(FORMATTER)
file_handler = logging.FileHandler(LOG_FILE)
file_handler.setFormatter(FORMATTER)

# set logger object
logger = logging.getLogger("unit_test")
logger.setLevel(logging.DEBUG)
logger.addHandler(console_handler)
logger.addHandler(file_handler)

# recreate data table input for testing
covid_data = pd.DataFrame(columns=["date", "iso", "num_confirmed", "num_deaths", "num_recovered"])


class MyTestCase(unittest.TestCase):
    
    def test_response_present(self):
        ind = 0  # replicating the loop input
        self.assertEqual(request_data(results=covid_data, date="2020-10-29", iso="AFG", ind=ind, logger_=logger), "found and appended")

    def test_response_absent(self):
        ind = 0  # replicating the loop input
        self.assertEqual(request_data(results=covid_data, date="2020-10-29", iso="ABW", ind=ind, logger_=logger), "empty")


if __name__ == '__main__':
    unittest.main()